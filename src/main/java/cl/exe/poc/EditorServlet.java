package cl.exe.poc;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EditorServlet
 */
public class EditorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// OBTENER TEXTO DESDE DB
		// AGREGAR TEXTO A RESPUESTA
		// MOSTRAR VISTA SEGUN PARAMETRO
		String probar = request.getParameter("probar");
		
		if ("tinymce".equals(probar)) {
			request.getRequestDispatcher("/tinymce.jsp").forward(request,response);
		}
		
		if ("trumbowyg".equals(probar)) {
			request.getRequestDispatcher("/trumbowyg.jsp").forward(request,response);
		}
		
		if ("ckeditor".equals(probar)) {
			request.getRequestDispatcher("/ckeditor.jsp").forward(request,response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
