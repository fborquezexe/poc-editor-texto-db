<html>
	<head>
		<script src="node_modules/ckeditor/plugins/lite/js/jquery.min.js"></script>
		<script src="node_modules/ckeditor/ckeditor.js"></script>
		<script src="node_modules/ckeditor/sample.js"></script>
		<script src="node_modules/ckeditor/plugins/lite/lite-interface.js"></script>	
	</head>
	<body>
		<h2>POC Editores de textos para Exedoc - CKEditor</h2>
		<a href="index.jsp">Volver</a>
		<p>
			<textarea id="editor"></textarea>
		</p>
		<script>
			initSample();
		</script>
	</body>
</html>
